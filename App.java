package sa.edu.kau.fcit.cpit252;


import java.util.Scanner;
import sa.edu.kau.fcit.cpit252.gallery.Gallery;
import sa.edu.kau.fcit.cpit252.gallery.Photo;
import sa.edu.kau.fcit.cpit252.sharing.Bluetooth;
import sa.edu.kau.fcit.cpit252.sharing.Whatsapp;
import sa.edu.kau.fcit.cpit252.sharing.Gmail;
import sa.edu.kau.fcit.cpit252.sharing.Messages;
import sa.edu.kau.fcit.cpit252.sharing.Sharetype;

public class App  {
    static Sharetype type;
    
    public static void main( String[] args )
    {
        Gallery gallery = new Gallery();
        Scanner in = new Scanner(System.in);
        
        Photo photo1 = new Photo("beach", "", "2022-10-28");
        Photo photo2 = new Photo("mountain", "",  "2022-10-28");

        gallery.addPhoto(photo1);
        gallery.addPhoto(photo2);
        
        
        
        System.out.print("select a sharing method (bluetooth, whatsApp, gmail, or messages): ");
        String choice = in.next();
        
        
        if (choice.equalsIgnoreCase("bluetooth")) {
            type = new Sharetype(new Bluetooth());
            type.executeStrategy("it sent via Bluetooth");
        } 
        else if (choice.equalsIgnoreCase("whatsApp")) {
            type = new Sharetype(new Whatsapp());
            type.executeStrategy("it sent via Whatsapp");
        } 
        else if (choice.equalsIgnoreCase("gmail")) {
            type = new Sharetype(new Gmail());
            type.executeStrategy("it sent via Gmail");
        } 
        else if (choice.equalsIgnoreCase("messages")) {
            type = new Sharetype(new Messages());
            type.executeStrategy("it sent via Messages");
        }

    }
}
