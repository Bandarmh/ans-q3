package sa.edu.kau.fcit.cpit252.gallery;

import sa.edu.kau.fcit.cpit252.sharing.Shareable;
import java.util.ArrayList;
import java.util.List;

public class Gallery {
    List<Photo> photos;
    public Gallery(){
        this.photos=new ArrayList<Photo>();
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void addPhoto(Photo photo){
        this.photos.add(photo);
    }

    public void removePhoto(Photo photo){
        this.photos.remove(photo);
    }

    public void share(Shareable shareable){
        for(Photo photo : this.photos) {
            shareable.share(photo.getPath());
        }
    }

    @Override
    public String toString(){
        String message ="Image Gallery:\n";
        for(Photo photo : this.photos){
            message += photo.toString() + "\n";
        }
        return message;
    }
}
