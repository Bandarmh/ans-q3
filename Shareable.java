package sa.edu.kau.fcit.cpit252.sharing;

public interface Shareable {
    public void share(String path);
}
