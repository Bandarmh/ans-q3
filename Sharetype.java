
package sa.edu.kau.fcit.cpit252.sharing;

public class Sharetype {
    
    private Shareable shareable;
    
    public Sharetype(Shareable shareable){ 
		this.shareable = shareable; 
	}

    public Sharetype() {
    }
    
   public void executeStrategy(String path){ 
		 shareable.share(path);
	} 


}
